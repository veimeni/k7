// KASUTATUD ON MATERJALE
//
// https://gist.github.com/AhmadElsagheer/d96d1081af545834e32f3624b06c762a
// ja
// https://codereview.stackexchange.com/questions/68716/permutations-of-any-given-numbers

import java.util.*;

public class Puzzle {

   static boolean[] usedNums;

   public static void main(String[] args) {

      ArrayList<String> letters = new ArrayList<>();
      HashMap<String, Integer> map = new HashMap();
      usedNums = new boolean[10];

      String[] w1 = args[0].split("");        // SEND
      String[] w2 = args[1].split("");        // MORE
      String[] w3 = args[2].split("");        // MONEY

      System.out.println("Does " + args[0] + " + " + args[1] + " = " + args[2] + "?\n");

      int i = 0;
      for (String word : args) {
         for (String letter : word.split("")) {
            if (!letters.contains(letter)) {
               letters.add(letter);
               map.put(letter, i);
               usedNums[i] = true;
               i++;
            }
         }
      }

      map.put(letters.get(0), 1);
      map.put(letters.get(1), 0);

      int result = 0;
      if (letters.size() != 10) {
         do {

            if (compute(map, letters, w1, w2, w3)) {
               printResult(map, letters);
               result++;
            }
            map = getNextMap(map, letters);

         } while (map != null);
      } else {
         int[] allNums = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
         List<List<Integer>> allPossibilites = getPermute(allNums);
         for (List<Integer> possibleNums : allPossibilites) {
            map = makeMap(map, possibleNums, letters);
            if (compute(map, letters, w1, w2, w3)) {
               result++;
            }
         }
      }

      System.out.println("-------------------------------");
      System.out.println("Total number of results: " + result);
      System.out.println("===============================\n\n");

   }

   //print result
   private static void printResult(HashMap<String, Integer> map, ArrayList<String> letters) {
      StringBuffer buffer = new StringBuffer();

      for (String key : map.keySet()) {
         buffer.append(key + "=" + map.get(key) + ", ");
      }
      buffer.deleteCharAt(buffer.length()-1);
      buffer.deleteCharAt(buffer.length()-1);
      System.out.println(buffer.toString());
   }

   //check if a + b == c
   private static boolean compute(HashMap<String, Integer> map, ArrayList<String> letters,
                                  String[] w1, String[] w2, String[] w3) {

      long a = valuateWord(map, letters, w1);
      long b = valuateWord(map, letters, w2);
      long c = valuateWord(map, letters, w3);

      return a + b == c;
   }

   //evaluate the word
   private static long valuateWord(HashMap<String, Integer> map, ArrayList<String> letters, String[] word) {
      long result = 0;

      if (map.get(word[0]) == 0) {
         return Long.MIN_VALUE;
      }

      for (String letter : word) {
         result = result * 10 + map.get(letter);
      }

      return result;
   }

   //add 1 to the last letter
   private static HashMap<String, Integer> getNextMap(HashMap<String, Integer> map, ArrayList<String> letters) {

      while (map.get(letters.get(0)) != 0) {
         int i = letters.size() - 1;

         String key = letters.get(i); // viimane täht
         usedNums[map.get(key)] = false; // vabastame koha

         for (int j = 1; j < 9; j++) {
            if ((map.get(key) + j) % 10 != 0) {
               if (!usedNums[(map.get(key) + j) % 10]) {
                  usedNums[(map.get(key) + j) % 10] = true;
                  map.put(key, (map.get(key) + j) % 10);
                  return fillRest(map, letters, i + 1);
               }
            } else {
               return findNextAvailable(map, letters, i - 1);
            }
         }


      }

      return null;
   }

   //if adding needs carry
   private static HashMap<String, Integer> findNextAvailable(HashMap<String, Integer> map,
                                                             ArrayList<String> letters, int index) {
      if (index == -1) return null;

      String key = letters.get(index);
      usedNums[map.get(key)] = false;

      for (int j = 1; j < 9; j++) {
         if ((map.get(key) + j) % 10 != 0) {
            if (!usedNums[(map.get(key) + j) % 10]) {
               usedNums[(map.get(key) + j) % 10] = true;
               map.put(key, (map.get(key) + j) % 10);
               return fillRest(map, letters, index + 1);
            }
         } else {
            return findNextAvailable(map, letters, index - 1);
         }
      }
      return map;
   }

   private static HashMap <String, Integer> fillRest (HashMap <String, Integer> map, ArrayList <String> letters, int index){
      for (int i = index; i < letters.size(); i++) {
         int available = 0;
         for (int j = 0; j < usedNums.length; j++) {
            if (!usedNums[j]) {
               available = j;
               usedNums[j] = true;
               break;
            }
         }

         map.put(letters.get(i), available);

      }
      return map;
   }

   // make all permutations
   private static List<List<Integer>> getPermute(int[] num) {
      List<List<Integer>> res=new ArrayList<List<Integer>>();
      res.add(new ArrayList<Integer>()); // Add an empty list
      for(int number = 0; number < num.length; number++)
      {
         List<List<Integer>> curr = new ArrayList<List<Integer>>();
         for(List<Integer> nestedL : res)
         {
            for(int j = 0; j < nestedL.size() + 1 ;j++)
            {
               nestedL.add(j,num[number]);
               List<Integer> temp = new ArrayList(nestedL);
               curr.add(temp);
               nestedL.remove(j);
            }
         }
         res = new ArrayList<List<Integer>>(curr);
      }

      return res;
   }

   // and convert it to a map
   private static HashMap<String, Integer> makeMap(HashMap <String, Integer> map,
                                                   List<Integer> numbers, ArrayList <String> letters) {
      for (int i = 0; i < 10; i++) {
         map.put(letters.get(i), numbers.get(i));
      }

      return map;
   }

}

